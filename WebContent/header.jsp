<header class="header navbar navbar-expand-lg navbar-dark bg-primary fixed-top">
	<a href="./index.jsp" class="navbar-brand mr-0 mr-2">
		<h4>Health Track</h4>
	</a>

	<ul class="navbar-nav flex-row ml-auto d-none d-sm-flex">
		<li class="nav-item">
			<a class="nav-link -navbar" href="./perfil.jsp" title="Perfil">
				<span>Perfil</span>
			</a>
		</li>
		<li class="nav-item">
			<a class="nav-link -navbar" href="./sair.jsp" title="Sair">
				<span>Sair</span>
			</a>
		</li>
	</ul>

	<div class="flex-row ml-auto d-block d-sm-none">
		<button class="toggle js-toggle-menu" type="button">
			<span class="bar"></span>
		</button>
	</div>
</header>