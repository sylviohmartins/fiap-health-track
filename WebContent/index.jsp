<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	
	<title>Home - JSPs</title>
	
	<link rel="stylesheet" type="text/css" href="resources/css/bootstrap-combined.no-icons.min.css" />
	<link rel="stylesheet" type="text/css" href="resources/css/font-awesome.css" />
	<link rel="stylesheet" type="text/css" href="resources/css/app.css" />
</head>
<body class="body -private">
	<%@ include file="header.jsp" %>

	<div class="container-fluid">
		<div class="row">
			<nav class="sidebar js-menu">
				<div class="sidebar-sticky">
					<ul class="nav flex-column">
						<li class="nav-item">
							<a href="./index.jsp" class="nav-link -sidebar -active" title="Dashboard">
								<span>Dashboard</span>
							</a>
						</li>
	
						<li class="nav-item">
							<a href="./peso.jsp" class="nav-link -sidebar" title="Meu Peso">
								<span>Meu Peso</span>
							</a>
						</li>
	
						<li class="nav-item">
							<a href="./pressao.jsp" class="nav-link -sidebar" title="Press�o Arterial">
								<span>Press�o Arterial</span>
							</a>
						</li>
						
						<li class="nav-item">
							<a href="./alimentacao.jsp"class="nav-link -sidebar" title="Alimenta��o">
								<span>Alimenta��o</span>
							</a>
						</li>
						
						<li class="nav-item">
							<a href="./exercicios.jsp" class="nav-link -sidebar" title="Exerc�cios">
								<span>Exerc�cios</span>
							</a>
						</li>
						
						<li class="nav-item">
							<a href="./glicemia.jsp" class="nav-link -sidebar" title="Glicemia"> 
								<span>Glicemia</span>
							</a>
						</li>
						
						<li class="nav-item d-block d-sm-none">
							<a href="./perfil.jsp" class="nav-link -sidebar" title="Perfil">
								<span>Perfil</span>
							</a>
						</li>
						
						<li class="nav-item d-block d-sm-none">
							<a href="./sair.jsp" class="nav-link -sidebar" title="Sair">
								<span>Sair</span>
							</a>
						</li>
					</ul>
				</div>
			</nav>
			
			<main role="main" class="ml-auto">
			    <section class="page -dashboard px-5">
			        <header class="page-header d-none">
			            <h2>Dashboard</h2>
			        </header>
			        
			        <article class="page-content">
			            <div class="row">
			                <div class="col-sm-12 mb-4">
			                    <div class="card -user">
			                        <div class="card-body">
			                            <div class="media">
			                                <div class="media-body">
			                                    <h3 class="card-title mb-0">Sylvio Martins</h3>
			                                    
			                                    <ul class="list-inline m-0">
			                                        <li class="list-inline-item">
			                                            <p class="card-text mb-0">22 anos</p>
			                                        </li>
			                                        
			                                        <li class="list-inline-item">
			                                            <p class="card-text mb-0">75 kilos</p>
			                                        </li>
			                                        
			                                        <li class="list-inline-item">
			                                            <p class="card-text mb-0">1.78 altura</p>
			                                        </li>
			                                    </ul>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			                
			                <div class="col-lg-4 mb-4">
			                    <div class="card -status">
			                        <div class="card-body">
			                            <h3 class="card-title">
			                                <span>IMC</span>
			                            </h3>
			                            <p class="card-text">26,7</p>
			                        </div>
			                        <div class="card-footer bg-warning">
			                            <small class="text text-white text-uppercase">Sobrepeso</small>
			                        </div>
			                    </div>
			                </div>
			                
			                <div class="col-lg-4 mb-4">
			                    <div class="card -status">
			                        <div class="card-body">
			                            <h3 class="card-title">
			                                <span>Press�o Arterial</span>
			                            </h3>
			                            <p class="card-text">120/80</p>
			                        </div>
			                        <div class="card-footer bg-success">
			                            <small class="text text-white text-uppercase">Normal</small>
			                        </div>
			                    </div>
			                </div>
			                
			                <div class="col-lg-4">
			                    <div class="card -status">
			                        <div class="card-body">
			                            <h3 class="card-title">
			                                <span>Glicose</span>
			                            </h3>
			                            <p class="card-text">200 mg/dl</p>
			                        </div>
			                        <div class="card-footer bg-danger">
			                            <small class="text text-white text-uppercase">Diab�tico</small>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </article>
			    </section>
			</main>
			
		</div>
	</div>

	<script type="text/javascript" src="resources/js/app.js"></script>
</body>
</html>