<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	
	<title>Redefinir senha - Health Track</title>
	
	<link rel="stylesheet" type="text/css" href="resources/css/app.css" />
</head>
<body class="body -public h-100">
	<main class="main" role="main">

    <section class="section -recover">
      <div class="container-fluid h-100">
        <article class="row h-100">
          <div class="col-md-8 offset-md-2 col-lg-6 offset-lg-3 h-100">
            <div class="d-flex align-items-center h-100">
              <div class="section-content w-100">
                <div class="row">
                  <div class="col-sm-8 offset-sm-2">
                    <h2 style="color: white">Health Track</h2>
                    <p class="text text-white">Para redefinir sua senha, informe abaixo</p>
                    <form action="./new-password.jsp" class="form mb-4 needs-validation" novalidate>
                      <div class="form-group">
                        <label for="email" class="form-label text-white">Seu e-mail</label>
                        <input type="email" name="email" class="form-control" id="email" required>
                      </div>
                      <button type="submit" class="btn btn-outline-light">Enviar</button>
                    </form>
                    <a href="./sair.jsp" class="link text-white fs-sm text text-uppercase" title="Lembrou? Fa�a login aqui">Lembrou? Fa�a login aqui</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </article>
      </div>
    </section>

  </main>

	<script type="text/javascript" src="resources/js/app.js"></script>
</body>
</html>