<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	
	<title>Peso - Health Track</title>
	
	<link rel="stylesheet" type="text/css" href="resources/css/bootstrap-combined.no-icons.min.css" />
	<link rel="stylesheet" type="text/css" href="resources/css/font-awesome.css" />
	<link rel="stylesheet" type="text/css" href="resources/css/app.css" />
</head>
<body class="body -private">
	<%@ include file="header.jsp" %>

	<div class="container-fluid">
		<div class="row">
			<nav class="sidebar js-menu">
				<div class="sidebar-sticky">
					<ul class="nav flex-column">
						<li class="nav-item">
							<a href="./index.jsp" class="nav-link -sidebar" title="Dashboard">
								<span>Dashboard</span>
							</a>
						</li>
	
						<li class="nav-item">
							<a href="./peso.jsp" class="nav-link -sidebar -active" title="Meu Peso">
								<span>Meu Peso</span>
							</a>
						</li>
	
						<li class="nav-item">
							<a href="./pressao.jsp" class="nav-link -sidebar" title="Press�o Arterial">
								<span>Press�o Arterial</span>
							</a>
						</li>
						
						<li class="nav-item">
							<a href="./alimentacao.jsp"class="nav-link -sidebar" title="Alimenta��o">
								<span>Alimenta��o</span>
							</a>
						</li>
						
						<li class="nav-item">
							<a href="./exercicios.jsp" class="nav-link -sidebar" title="Exerc�cios">
								<span>Exerc�cios</span>
							</a>
						</li>
						
						<li class="nav-item">
							<a href="./glicemia.jsp" class="nav-link -sidebar" title="Glicemia"> 
								<span>Glicemia</span>
							</a>
						</li>
						
						<li class="nav-item d-block d-sm-none">
							<a href="./perfil.jsp" class="nav-link -sidebar" title="Perfil">
								<span>Perfil</span>
							</a>
						</li>
						
						<li class="nav-item d-block d-sm-none">
							<a href="./sair.jsp" class="nav-link -sidebar" title="Sair">
								<span>Sair</span>
							</a>
						</li>
					</ul>
				</div>
			</nav>

			<main role="main" class="ml-auto">
        <section class="page -dashboard px-5">
          <header class="page-header mb-4">
            <div class="row">
              <div class="col-9 col-sm-6">
                <h2 class="page-title">Meu Peso</h2>
              </div>
              <div class="col-3 col-sm-6 text-right">
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-create">
                  <span>Adicionar</span>
                </button>
              </div>
            </div>
          </header>
          <article class="page-content">
            <div class="card mb-5">
              <div class="card-body p-0">
                <table class="table table-borderless table-striped mb-0">
                  <thead class="table-thead">
                    <tr class="table-row">
                      <th class="table-col" scope="col">Data da pesagem</th>
                      <th class="table-col" scope="col">Peso</th>
                      <th class="table-col" scope="col">A��es</th>
                    </tr>
                  </thead>
                  <tbody class="table-tbody">
                    <tr class="table-row">
                      <td class="table-col" data-label="Data da pesagem">
                        20/07/2019
                      </td>
                      <td class="table-col" data-label="Peso">
                        88,5 kg
                      </td>
                      <td class="table-col" data-label="A��es">
                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-edit">
                          <span>Editar</span>
                        </button>
                        <a href="#" class="btn btn-danger btn-sm" title="Excluir">
                          <span>Excluir</span>
                        </a>
                      </td>
                    </tr>
                    <tr class="table-row">
                      <td class="table-col" data-label="Data da pesagem">
                        17/07/2019
                      </td>
                      <td class="table-col" data-label="Peso">
                        88,3 kg
                      </td>
                      <td class="table-col" data-label="A��es">
                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-edit">
                          <span>Editar</span>
                        </button>
                        <a href="#" class="btn btn-danger btn-sm" title="Excluir">
                          <span>Excluir</span>
                        </a>
                      </td>
                    </tr>
                    <tr class="table-row">
                      <td class="table-col" data-label="Data da pesagem">
                        01/07/2019
                      </td>
                      <td class="table-col" data-label="Peso">
                        89,1 kg
                      </td>
                      <td class="table-col" data-label="A��es">
                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-edit">
                          <span>Editar</span>
                        </button>
                        <a href="#" class="btn btn-danger btn-sm" title="Excluir">
                          <span>Excluir</span>
                        </a>
                      </td>
                    </tr>
                    <tr class="table-row">
                      <td class="table-col" data-label="Data da pesagem">
                        29/06/2019
                      </td>
                      <td class="table-col" data-label="Peso">
                        90,5 kg
                      </td>
                      <td class="table-col" data-label="A��es">
                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-edit">
                          <span>Editar</span>
                        </button>
                        <a href="#" class="btn btn-danger btn-sm" title="Excluir">
                          <span>Excluir</span>
                        </a>
                      </td>
                    </tr>
                    <tr class="table-row">
                      <td class="table-col" data-label="Data da pesagem">
                        19/06/2019
                      </td>
                      <td class="table-col" data-label="Peso">
                        88,4 kg
                      </td>
                      <td class="table-col" data-label="A��es">
                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-edit">
                          <span>Editar</span>
                        </button>
                        <a href="#" class="btn btn-danger btn-sm" title="Excluir">
                          <span>Excluir</span>
                        </a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

            <nav aria-label="Page navigation">
              <ul class="pagination justify-content-center">
                <li class="page-item">
                  <a class="page-link" href="#" aria-label="Anterior">
                    <span aria-hidden="true">&laquo;</span>
                  </a>
                </li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item active"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                  <a class="page-link" href="#" aria-label="Pr�xima">
                    <span aria-hidden="true">&raquo;</span>
                  </a>
                </li>
              </ul>
            </nav>
          </article>
        </section>
      </main>
    </div>
  </div>

  <div class="modal fade" id="modal-create" tabindex="-1" role="dialog" aria-labelledby="modal-create-title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content bg-transparent">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white font-weight-bold" id="modal-create-title">Registrar Peso</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body bg-white">
          <form action="#" class="form needs-validation" id="form-create" novalidate>
            <div class="form-row">
              <div class="form-group col-sm-6">
                <label for="date" class="form-label">Data da pesagem</label>
                <input type="text" name="date" class="form-control js-datepicker" id="date" required>
              </div>
              <div class="form-group col-sm-6">
                <label for="weight" class="form-label">Peso</label>
                <input type="text" name="weight" class="form-control" id="weight" required>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer bg-white">
          <button type="button" class="btn btn-danger" data-dismiss="modal">
            <span>Cancelar</span>
          </button>
          <button type="submit" form="form-create" class="btn btn-success">
            <span>Salvar</span>
          </button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="modal-create-title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content bg-transparent">
        <div class="modal-header bg-primary">
          <h5 class="modal-title text-white font-weight-bold" id="modal-create-title">Editar Peso</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body bg-white">
          <form action="#" class="form needs-validation" id="form-edit" novalidate>
            <div class="form-row">
              <div class="form-group col-sm-6">
                <label for="date" class="form-label">Data da pesagem</label>
                <input type="text" value="20/07/2019" name="date" class="form-control js-datepicker" id="date" required>
              </div>
              <div class="form-group col-sm-6">
                <label for="weight" class="form-label">Peso</label>
                <input type="text" value="88.5" name="weight" class="form-control" id="weight" required>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer bg-white">
          <button type="button" class="btn btn-danger" data-dismiss="modal">
            <span>Cancelar</span>
          </button>
          <button type="submit" form="form-edit" class="btn btn-success">
            <span>Salvar</span>
          </button>
        </div>
      </div>
    </div>
  </div>

	<script type="text/javascript" src="resources/js/app.js"></script>
</body>
</html>