<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	
	<title>Sair - Health Track</title>
	
	<link rel="stylesheet" type="text/css" href="resources/css/bootstrap-combined.no-icons.min.css" />
	<link rel="stylesheet" type="text/css" href="resources/css/font-awesome.css" />
	<link rel="stylesheet" type="text/css" href="resources/css/app.css" />
</head>
<body class="body -public h-100">
	<main class="main" role="main">

    <section class="section -login">
      <div class="container-fluid h-100">
        <article class="row h-100">
          <div class="col-md-8 order-md-2 col-lg-6 order-lg-3 h-100">
            <div class="d-flex align-items-center h-100">
              <div class="section-content w-100">
                <div class="row">
                  <div class="col-sm-8 offset-sm-2">
                    <h2 class="brand -default h-bg-contain mb-4">Health Track</h2>
                    <form action="./index.jsp" class="form needs-validation mb-4" novalidate>
                      <div class="form-group">
                        <label for="login" class="form-label">E-mail</label>
                        <input type="email" name="login" class="form-control" id="login" placeholder="seuemail@fiap.com.br" required>
                      </div>
                      <div class="form-group">
                        <label for="password" class="form-label">Senha</label>
                        <input type="password" name="password" class="form-control" id="password" required>
                      </div>
                      <div class="row mb-4">
                        <div class="col-12 col-xl-7">
                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="remember" id="remember" value="1">
                            <label for="remember" class="custom-control-label fs-sm text text-uppercase">Continuar conectado</label>
                          </div>
                        </div>
                        <div class="col-12 col-xl-5 text-xl-right">
                          <a href="./redefinir-senha.jsp" class="link fs-sm text text-uppercase" title="Esqueceu a senha?">Esqueceu a senha?</a>
                        </div>
                      </div>
                      <button type="submit" class="btn btn-primary">Acessar</button>
                    </form>
                    <a href="./registrar.jsp" class="link fs-sm text text-uppercase" title="Ainda n�o possui conta? Cadastre-se">Ainda n�o possui conta? Cadastre-se</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 order-md-0 col-lg-6 order-lg-0 h-100 p-0 d-none d-md-block">
            <div class="background-area h-bg-multiply h-bg-cover w-100 h-100" style="background-image: url('resources/images/logo-fiap.jpg')"></div>
          </div>
        </article>
      </div>
    </section>

  </main>

	<script type="text/javascript" src="resources/js/app.js"></script>
</body>
</html>