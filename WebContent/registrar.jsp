<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	
	<title>Registrar - Health Track</title>
	
	<link rel="stylesheet" type="text/css" href="resources/css/bootstrap-combined.no-icons.min.css" />
	<link rel="stylesheet" type="text/css" href="resources/css/font-awesome.css" />
	<link rel="stylesheet" type="text/css" href="resources/css/app.css" />
</head>
<body class="body -public h-100">
	<main class="main" role="main">

    <section class="section -register">
      <div class="container-fluid h-100">
        <article class="row h-100">
          <div class="col-md-8 offset-md-2 col-lg-6 offset-lg-3 h-100">
            <div class="d-flex align-items-center h-100">
              <div class="section-content w-100">
                <div class="row">
                  <div class="col-sm-8 offset-sm-2">
                    <h2>Health Track</h2>
                    <form action="./dashboard.jsp" class="form mb-4 needs-validation" novalidate>
                      <div class="form-row">
                        <div class="form-group col-sm-12">
                          <label for="name" class="form-label text-white">Nome</label>
                          <input type="text" name="name" class="form-control" id="name" required>
                        </div>
                        <div class="form-group col-sm-6">
                          <label for="birthdate" class="form-label text-white">Nascimento</label>
                          <input type="text" name="birthdate" class="form-control js-datepicker" id="birthdate" required>
                        </div>
                        <div class="form-group col-sm-6">
                          <label for="birthdate" class="form-label text-white">G�nero</label>
                          <select class="custom-select" required>
                            <option value="" selected>Selecione</option>
                            <option value="1">Masculino</option>
                            <option value="2">Feminino</option>
                          </select>
                        </div>
                        <div class="form-group col-sm-12">
                          <label for="size" class="form-label text-white">Altura</label>
                          <input type="range" class="custom-range" step="1" min="0" max="250" id="size" required>
                        </div>
                        <div class="form-group col-sm-12">
                          <label for="email" class="form-label text-white">E-mail</label>
                          <input type="email" name="email" class="form-control" id="email" required>
                        </div>
                        <div class="form-group col-sm-6">
                          <label for="password" class="form-label text-white">Senha</label>
                          <input type="password" name="password" class="form-control" id="password" required>
                        </div>
                        <div class="form-group col-sm-6">
                          <label for="password_confirm" class="form-label text-white">Confirmar Senha</label>
                          <input type="password" name="password_confirm" class="form-control" id="password_confirm" required>
                        </div>
                      </div>
                      <button type="submit" class="btn btn-outline-light">Registrar</button>
                    </form>
                    <a href="./sair.jsp" class="link text-white fs-sm text text-uppercase" title="J� possui cadastro? Fa�a login aqui">J� possui cadastro? Fa�a login aqui</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </article>
      </div>
    </section>

  </main>

	<script type="text/javascript" src="resources/js/app.js"></script>
</body>
</html>