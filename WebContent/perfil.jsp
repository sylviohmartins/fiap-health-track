<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	
	<title>Perfil - Health Track</title>
	
	<link rel="stylesheet" type="text/css" href="resources/css/bootstrap-combined.no-icons.min.css" />
	<link rel="stylesheet" type="text/css" href="resources/css/font-awesome.css" />
	<link rel="stylesheet" type="text/css" href="resources/css/app.css" />
</head>
<body class="body -private">
	<%@ include file="header.jsp" %>

	<div class="container-fluid">
		<div class="row">
			<nav class="sidebar js-menu">
				<div class="sidebar-sticky">
					<ul class="nav flex-column">
						<li class="nav-item">
							<a href="./index.jsp" class="nav-link -sidebar" title="Dashboard">
								<span>Dashboard</span>
							</a>
						</li>
	
						<li class="nav-item">
							<a href="./peso.jsp" class="nav-link -sidebar" title="Meu Peso">
								<span>Meu Peso</span>
							</a>
						</li>
	
						<li class="nav-item">
							<a href="./pressao.jsp" class="nav-link -sidebar" title="Press�o Arterial">
								<span>Press�o Arterial</span>
							</a>
						</li>
						
						<li class="nav-item">
							<a href="./alimentacao.jsp"class="nav-link -sidebar" title="Alimenta��o">
								<span>Alimenta��o</span>
							</a>
						</li>
						
						<li class="nav-item">
							<a href="./exercicios.jsp" class="nav-link -sidebar -active" title="Exerc�cios">
								<span>Exerc�cios</span>
							</a>
						</li>
						
						<li class="nav-item">
							<a href="./glicemia.jsp" class="nav-link -sidebar" title="Glicemia"> 
								<span>Glicemia</span>
							</a>
						</li>
						
						<li class="nav-item d-block d-sm-none">
							<a href="./perfil.jsp" class="nav-link -sidebar" title="Perfil">
								<span>Perfil</span>
							</a>
						</li>
						
						<li class="nav-item d-block d-sm-none">
							<a href="./sair.jsp" class="nav-link -sidebar" title="Sair">
								<span>Sair</span>
							</a>
						</li>
					</ul>
				</div>
			</nav>

			<main role="main" class="ml-auto">
        <section class="page -dashboard px-5">
          <header class="page-header mb-4">
            <h2 class="page-title">Meu Perfil</h2>
          </header>

          <article class="page-content">
            <div class="row">
              <div class="col-sm-12 col-lg-4 col-xl-3">
                <div class="card -user mb-4">
                  <div class="card-body">
                    <div class="media d-block">
                      <form action="./profile.html" class="form" novalidate>
                        <div class="avatar-box">
                          <input id="avatar-input" class="avatar-input" type="file" name="avatar" accept="image/*">
                          <label for="avatar-input" class="avatar -bigger avatar-label h-bg-cover align-justify-center m-auto"></label>
                        </div>
                      </form>
                      <div class="media-body text-center p-3">
                        <h3 class="card-title">Sylvio Martins</h3>
                        <ul class="list-unstyled mb-0">
                          <li class="list-item">
                            <p class="card-text mb-2">22 anos</p>
                          </li>
                          <li class="list-item">
                            <p class="card-text mb-2">75 kilos</p>
                          </li>
                          <li class="list-item">
                            <p class="card-text mb-0">1.78 altura</p>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-12 col-lg-8 col-xl-9">
                <div class="card">
                  <div class="card-body">
                    <form action="./profile.html" class="form needs-validation" novalidate>
                      <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="name" class="form-label">Nome</label>
                          <input type="text" name="name" class="form-control" id="name" value="Sylvio Martins" required>
                        </div>
                        <div class="form-group col-sm-6 col-md-3">
                          <label for="birthdate" class="form-label">Nascimento</label>
                          <input type="text" name="birthdate" class="form-control js-datepicker" value="05/06/1997" id="birthdate" required>
                        </div>
                        <div class="form-group col-sm-6 col-md-3">
                          <label for="birthdate" class="form-label">G�nero</label>
                          <select class="custom-select" required>
                            <option value="">Selecione</option>
                            <option value="1" selected>Masculino</option>
                            <option value="2">Feminino</option>
                          </select>
                        </div>
                        <div class="form-group col-sm-12">
                          <label for="size" class="form-label">Altura</label>
                          <input type="range" class="custom-range" step="1" min="0" max="250" id="size">
                        </div>
                        <div class="form-group col-md-4">
                          <label for="email" class="form-label">E-mail</label>
                          <input type="email" name="email" class="form-control" id="email" required>
                        </div>
                        <div class="form-group col-sm-6 col-md-4">
                          <label for="password" class="form-label">Senha</label>
                          <input type="password" name="password" class="form-control" id="password">
                        </div>
                        <div class="form-group col-sm-6 col-md-4">
                          <label for="password_confirm" class="form-label">Confirmar Senha</label>
                          <input type="password" name="password_confirm" class="form-control" id="password_confirm">
                        </div>
                      </div>
                      <div class="text-right">
                        <button type="submit" class="btn btn-success">Salvar</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </article>
        </section>
      </main>
    </div>
  </div>

	<script type="text/javascript" src="resources/js/app.js"></script>
</body>
</html>